export class User {
    username?: string;
    password?: string;
    email?: string;
    role: string[];
    name?: string;

    constructor(username: string, password: string, email: string, name: string ) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.role = ['user'];

    }

}
