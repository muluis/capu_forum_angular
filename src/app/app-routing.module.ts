import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewComponentComponent } from './new-component/new-component.component';

const routes: Routes = [

  {
    path: 'home', component: NewComponentComponent
  },
  // {
  //   path: '', redirectTo: '/home', pathMatch: 'full'
  // }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
