import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-new-component',
  templateUrl: './new-component.component.html',
  styleUrls: ['./new-component.component.css']
})
export class NewComponentComponent implements OnInit {
    constructor (private route: ActivatedRoute, private loginService: LoginService) {

    }

// https://www.npmjs.com/package/ngx-quill website.
  @Input() test: any;
  editorGroup: FormGroup;
  editorContent: any;

  post: any;
  myPosts: any[] = [];
  showPosts = false;


  customColor: '#00bfff';

  style = {
    height: '100px',
    width: 'auto'
  };


  config = {
    // toolbar: ['bold', 'italic', 'underline', 'strike', {color: []}]
  };


  ngOnInit() {
    console.log(this.route.snapshot.queryParamMap.get('test'));
  this.editorGroup = new FormGroup({
    'editor': new FormControl(null)
});
  console.log(this.test);
  }


  testBackEnd(e) {
    this.loginService.signUp().subscribe ((data) => console.log(data));
  }

  onSubmit(e) {
    this.editorContent = this.editorGroup.get('editor').value;
    this.myPosts.push(this.editorGroup.get('editor').value);
    console.log(this.myPosts);
  }

  showPostsAction() {
    this.showPosts = true;

  }
}
