import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JwtResponse } from '../models/JwtResponse';
import {User} from '../models/User';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private loginUrl = 'http://localhost:8080/api/auth/signin';
  private signupUrl = 'http://localhost:8082/api/auth/signup';

  constructor(private http: HttpClient) { }


  attemptAuth(credentials: any): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.loginUrl, credentials, httpOptions);
  }

  signUp() {

    const test = new User('Luzira', 'pizza123', 'this@this.com', 'Luis');

    console.log(JSON.stringify(test));
    return this.http.post(this.signupUrl, test);
  }

}
